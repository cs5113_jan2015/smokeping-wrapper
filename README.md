# README #

SMOKEPING WRAPPER

### INTRODUCTION ###

* The purpose of this project is to visualize the LOSS RATES and RTT values from multiple devices on same page instead of checking these values for each device individually in SmokePing.


### Configuration/Requirements ###

* SmokePing server hosted on system.
* Python installed on the web server and the SmokePing server.


### Installation ###

* Host  “SmokePingWrapper” on a web server.
* Run the getxml.sh file.
* Run generateRTT.sh file

### Contact ###

* [Utpal Bora](mailto:CS14MTECH11017@iith.ac.in)
* [Pravi Malviya](mailto:CS14MTECH11012@iith.ac.in)
* [Ankit Rathore](mailto:CS14MTECH11001@iith.ac.in)